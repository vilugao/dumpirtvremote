#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>

#include "uart.h"
#include "ircapt.h"

int main(void)
{
  PRR = _BV(PRTWI) | _BV(PRTIM2) | _BV(PRSPI) | _BV(PRADC);
  DIDR0 = _BV(ADC0D) | _BV(ADC1D) | _BV(ADC2D) | _BV(ADC3D)
          | _BV(ADC4D) | _BV(ADC5D);
  DIDR1 = _BV(AIN0D) | _BV(AIN1D);
  ACSR = _BV(ACD);

#if F_CPU == 16000000L
  CLKPR = _BV(CLKPCE);
  CLKPR = _BV(CLKPS2); // F_CPU /= 16;
#elif F_CPU == 8000000L
  CLKPR = _BV(CLKPCE);
  CLKPR = _BV(CLKPS0) | _BV(CLKPS1); // F_CPU /= 8;
#endif

  PORTB |= _BV(PB0);
  DDRB |= _BV(PB5);
  TIMSK1 = _BV(ICIE1);

  uart_init();
  uart_sendstr_P(PSTR("Pronto.\r\n"));

  SMCR = _BV(SE);
  sei();
  while (1)
    sleep_cpu();
}

static void sendhex32(unsigned long v) {
  const char d = v & 0xF;
  if (v >>= 4)
    sendhex32(v);
  uart_sendchar(d + (d >= 0xA ? 'A' - 0xA : '0'));
}

ISR(TIMER1_CAPT_vect, ISR_NAKED)
{
  PORTB |= _BV(PB5);
  const unsigned long ircode = ircapt_read(IRCAPT_CLKDIV64);
  PORTB &= ~_BV(PB5);
  sendhex32(ircode);
  uart_sendchar('h');
  uart_sendchar(' ');
  sendhex32(ircapt_necaddr(ircode));
  uart_sendchar('h');
  uart_sendchar(',');
  sendhex32(ircapt_neccmd(ircode));
  uart_sendchar('h');
  uart_sendnl();
  TIFR1 |= _BV(ICF1);
  reti();
}

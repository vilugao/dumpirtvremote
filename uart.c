#include <avr/io.h>
#include <avr/pgmspace.h>

#include "uart.h"

#if (F_CPU == 1000000UL || F_CPU % 8000000UL == 0)
#define BAUD (9600 * (F_CPU / 1000000UL))
#else
#error "Requer F_CPU = 1 MHz, 8 MHz or 16 MHz."
#endif

void uart_init(void)
{
#include <util/setbaud.h>
  UBRR0 = UBRR_VALUE;
#if USE_2X
  UCSR0A = _BV(U2X0);
#else
  UCSR0A = 0;
#endif
  UCSR0B = _BV(TXEN0);
  UCSR0C = _BV(UCSZ01) | _BV(UCSZ00);
}

void uart_sendchar(const char c)
{
  loop_until_bit_is_set(UCSR0A, UDRE0);
  UDR0 = c;
}

void uart_sendstr_P(PGM_P pstr)
{
  char c;
  while ((c = pgm_read_byte(pstr++)) != 0)
    uart_sendchar(c);
}

void uart_sendnl(void)
{
  uart_sendchar('\r');
  uart_sendchar('\n');
}

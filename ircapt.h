#define IRCAPT_CLKDIV8 _BV(CS01)
#define IRCAPT_CLKDIV64 (_BV(CS00) | _BV(CS01))
#define IRCAPT_CLKDIV256 _BV(CS02)
#define IRCAPT_CLKDIV1024 (_BV(CS00) | _BV(CS02))

unsigned long ircapt_read(uint8_t);

#define ircapt_necaddr(ircode) ((unsigned short)(ircode))
unsigned char ircapt_neccmd(unsigned long);

#include <avr/io.h>

#include "ircapt.h"

/* PB0 = Arduino digital pin 8. */
#define PIN_IRRECV PINB0

unsigned long ircapt_read(const uint8_t clkopt)
{
  loop_until_bit_is_clear(PINB, PIN_IRRECV);
  // Pula o pulso inicial.
  TCCR0B = clkopt;
  TCNT0 = 0;
  TIFR0 |= _BV(TOV0);
  unsigned long ircode = 0;
  while (bit_is_clear(PINB, PIN_IRRECV))
    if (bit_is_set(TIFR0, TOV0))
      goto endcapt;
  TCNT0 = 0;
  while (bit_is_set(PINB, PIN_IRRECV))
    if (bit_is_set(TIFR0, TOV0))
      goto endcapt;
  TCNT0 = 0;

  while (1) {
    while (bit_is_clear(PINB, PIN_IRRECV))
      if (bit_is_set(TIFR0, TOV0))
        goto endcapt;
    const uint8_t th = 3 * TCNT0;

    while (bit_is_set(PINB, PIN_IRRECV))
      if (bit_is_set(TIFR0, TOV0))
        goto endcapt;
    const uint8_t tt = TCNT0;
    TCNT0 = 0;
    ircode >>= 1;
    if (tt > th)
      ircode |= 0x80000000UL;
  }

endcapt:
  TCCR0B = 0;
  return ircode;
}

unsigned char ircapt_neccmd(const unsigned long ircode)
{
  const uint8_t cmd = ircode >> 16;
  const uint8_t cmdinv = ~(ircode >> 24);
  return cmdinv == cmd ? cmd : 0;
}
